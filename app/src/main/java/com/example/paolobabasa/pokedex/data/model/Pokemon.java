package com.example.paolobabasa.pokedex.data.model;

/**
 * Created by paolobabasa on 08/07/2018.
 */

public class Pokemon {
    private String url, name;

    public Pokemon(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
