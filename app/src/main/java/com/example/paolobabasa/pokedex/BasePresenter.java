package com.example.paolobabasa.pokedex;

/**
 * Created by paolobabasa on 08/07/2018.
 */

public interface BasePresenter {

    void Start();

    void Destroy();
}
