package com.example.paolobabasa.pokedex.api;

import com.example.paolobabasa.pokedex.data.model.Pokemon;
import com.example.paolobabasa.pokedex.data.model.PokemonList;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

/**
 * Created by paolobabasa on 08/07/2018.
 */

public interface PokemonService {
    @GET("pokemon")
    Single<PokemonList> pokemonList();
}
