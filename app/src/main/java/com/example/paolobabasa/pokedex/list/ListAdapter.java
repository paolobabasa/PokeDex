package com.example.paolobabasa.pokedex.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.paolobabasa.pokedex.R;
import com.example.paolobabasa.pokedex.data.model.Pokemon;
import com.example.paolobabasa.pokedex.data.model.PokemonList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by paolobabasa on 08/07/2018.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {
    private List<Pokemon> pokemonLists ;

    public ListAdapter() {
        this.pokemonLists = new ArrayList<>();
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
         View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pokemon_card_list, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        holder.getItemView().bindTo(pokemonLists.get(position));
    }

    public void setPokemonLists(List<Pokemon> pokemonLists) {
        this.pokemonLists = pokemonLists;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return pokemonLists.size() >= 0 ? pokemonLists.size() : 0;
    }

    class ListViewHolder extends RecyclerView.ViewHolder {


        ListViewHolder(View itemView) {
            super(itemView);
        }

        ListItemView getItemView() {
            return (ListItemView) super.itemView;
        }


    }


}
