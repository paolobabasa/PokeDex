package com.example.paolobabasa.pokedex;

import android.app.Application;

import com.example.paolobabasa.pokedex.api.PokemonService;
import com.example.paolobabasa.pokedex.helpers.RetrofitClient;

/**
 * Created by mynddynamicios on 10/07/2018.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        RetrofitClient retrofitClient = new RetrofitClient();
    }

    public static PokemonService getPokemonService() {
        return RetrofitClient.returnRetrofit().create(PokemonService.class);
    }
}
