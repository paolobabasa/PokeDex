package com.example.paolobabasa.pokedex.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by paolobabasa on 08/07/2018.
 */

public class PokemonList {
    @SerializedName("results")
    private List<Pokemon> results;

    public PokemonList(List<Pokemon> results) {
        this.results = results;
    }

    public List<Pokemon> getResults() {
        return results;
    }

    public void setResults(List<Pokemon> results) {
        this.results = results;
    }
}
